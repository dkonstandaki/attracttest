package com.dk.attracttest;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.dk.attracttest.fragment.ItemDetailsFragment;
import com.dk.attracttest.fragment.ItemsFragment;
import com.dk.attracttest.model.Item;

public class DetailsActivity extends AppCompatActivity {

    Item[] mItems;
    private FragmentPagerAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        int selectedIdx = 0;
        int selectedId = (int) getIntent().getLongExtra(ItemsFragment.KEY_ITEM_ID, 0);
        Parcelable[] parcelables = getIntent().getParcelableArrayExtra(ItemsFragment.KEY_ITEMS);
        mItems = new Item[parcelables.length];
        for (int i = 0 ; i < parcelables.length; i++) {
            mItems[i] = (Item) parcelables[i];
            if (mItems[i].itemId == selectedId)
                selectedIdx = i;
        }

        ViewPager vpPager = (ViewPager) findViewById(R.id.vpPager);
        mAdapter = new ItemsPagerAdapter(getSupportFragmentManager());
        vpPager.setAdapter(mAdapter);
        vpPager.setCurrentItem(selectedIdx);
    }

    private class ItemsPagerAdapter extends FragmentPagerAdapter {
        public ItemsPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return mItems.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mItems[position].name;
        }

        @Override
        public Fragment getItem(int position) {
            return ItemDetailsFragment.newInstance(mItems[position]);
        }
    }
}
