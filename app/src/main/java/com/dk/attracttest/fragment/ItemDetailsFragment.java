package com.dk.attracttest.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dk.attracttest.R;
import com.dk.attracttest.model.Item;

import java.lang.ref.WeakReference;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ItemDetailsFragment extends Fragment {

    public static final String KEY_ITEM = "item";

    private static final String TIME_FORMAT = "dd-MMMM-yyyy HH:mm";

    private SimpleDateFormat sdf = new SimpleDateFormat(TIME_FORMAT);

    private Item item;

    private LruCache<String, Bitmap> mMemoryCache;

    public static ItemDetailsFragment newInstance(Item item) {
        ItemDetailsFragment detailsFragment = new ItemDetailsFragment();
        Bundle args = new Bundle();
        args.putParcelable(KEY_ITEM, item);
        detailsFragment.setArguments(args);
        return detailsFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        item = getArguments().getParcelable(KEY_ITEM);

        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 8;
        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                return bitmap.getByteCount() / 1024;
            }
        };
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_details, container, false);
        ImageView ivImage = (ImageView) view.findViewById(R.id.ivDetailsImage);
        TextView tvName = (TextView) view.findViewById(R.id.tvDetailsName);
        TextView tvTime = (TextView) view.findViewById(R.id.tvDetailsTime);
        TextView tvDescription = (TextView) view.findViewById(R.id.tvDetailsDescription);
        loadBitmap(item.image, ivImage);
        tvName.setText(item.name);
        tvTime.setText(sdf.format(new Date(item.time)));
        tvDescription.setText(item.description);
        return view;
    }

    public void loadBitmap(String url, ImageView imageView) {
        final Bitmap bitmap = mMemoryCache.get(url);
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        } else {
            BitmapWorkerTask task = new BitmapWorkerTask(imageView);
            task.execute(url);
        }
    }

    private class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap> {
        private final WeakReference<ImageView> imageViewReference;

        public BitmapWorkerTask(ImageView imageView) {
            imageViewReference = new WeakReference<ImageView>(imageView);
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap bmp = null;
            String imageUrl = params[0];
            try {
                URL url = new URL(imageUrl);
                bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                addBitmapToMemoryCache(params[0], bmp);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bmp;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (imageViewReference != null && bitmap != null) {
                final ImageView imageView = imageViewReference.get();
                if (imageView != null) {
                    imageView.setImageBitmap(bitmap);
                }
            }
        }

        public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
            if (getBitmapFromMemCache(key) == null) {
                mMemoryCache.put(key, bitmap);
            }
        }

        public Bitmap getBitmapFromMemCache(String key) {
            return mMemoryCache.get(key);
        }
    }
}
