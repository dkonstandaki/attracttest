package com.dk.attracttest.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.dk.attracttest.DetailsActivity;
import com.dk.attracttest.R;
import com.dk.attracttest.model.Item;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;

public class ItemsFragment extends ListFragment {

    public static final String KEY_ITEMS = "items";
    public static final String KEY_ITEM_ID = "item_id";

    private static final String SERVER_URL = "http://others.php-cd.attractgroup.com/test.json";
    private static final String TAG_REQUEST_FAILED = "REQUEST_FAILED";

    private GetItemsTask mTask;
    private ItemsAdapter mAdapter;
    private Item[] mItems;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
        mTask = new GetItemsTask();
        mTask.execute();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (mAdapter != null)
                    mAdapter.getFilter().filter(newText);
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Intent detailsIntent = new Intent(getActivity(), DetailsActivity.class);
        detailsIntent.putExtra(KEY_ITEMS, mItems);
        detailsIntent.putExtra(KEY_ITEM_ID, id);
        getActivity().startActivity(detailsIntent);
    }

    private class GetItemsTask extends AsyncTask<Void, Integer, Void> {

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Void doInBackground(Void... params) {
            InputStream is = null;
            try {
                URL url = new URL(SERVER_URL);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(15000);
                connection.setRequestMethod("GET");
                connection.connect();
                int responseCode = connection.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    is = connection.getInputStream();
                    mItems = readItems(is);
                }
                connection.disconnect();
            } catch (Exception e) {
                Log.d(TAG_REQUEST_FAILED, e.getMessage());
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        @Override
        protected void onCancelled() {
        }

        @Override
        protected void onPostExecute(Void result) {
            if (mItems != null && mItems.length > 0) {
                mAdapter = new ItemsAdapter(getActivity(), Arrays.asList(mItems));
                setListAdapter(mAdapter);
            }
        }

        public Item[] readItems(InputStream stream) throws Exception {
            Item[] items = null;
            if (stream != null) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                StringBuilder result = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null)
                    result.append(line);
                String content = result.toString();
                if (!TextUtils.isEmpty(content)) {
                    JSONArray jsonItems = new JSONArray(content);
                    items = new Item[jsonItems.length()];
                    for (int i = 0; i < jsonItems.length(); i++)
                        items[i] = new Item(jsonItems.getJSONObject(i));
                }
            }
            return items;
        }
    }
}
