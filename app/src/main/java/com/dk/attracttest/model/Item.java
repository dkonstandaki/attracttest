package com.dk.attracttest.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

public class Item implements Parcelable {
    public int itemId;
    public String name;
    public String image;
    public long time;
    public String description;

    public Item(JSONObject jsonObject) {
        itemId = jsonObject.optInt("itemId");
        name = jsonObject.optString("name");
        image = jsonObject.optString("image");
        time = jsonObject.optLong("time");
        description = jsonObject.optString("description");
    }

    private Item(Parcel in) {
        itemId = in.readInt();
        name = in.readString();
        image = in.readString();
        time = in.readLong();
        description = in.readString();
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(itemId);
        out.writeString(name);
        out.writeString(image);
        out.writeLong(time);
        out.writeString(description);
    }

    public static final Parcelable.Creator<Item> CREATOR = new Parcelable.Creator<Item>() {
        public Item createFromParcel(Parcel in) {
            return new Item(in);
        }
        public Item[] newArray(int size) {
            return new Item[size];
        }
    };

    public int describeContents() {
        return 0;
    }
}
